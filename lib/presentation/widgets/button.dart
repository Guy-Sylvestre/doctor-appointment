import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({Key? key, required this.width, required this.title, required this.press}) : super(key: key);

  final double width;
  final String title;
  final Function press;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: ElevatedButton(
          onPressed: () => press(),
          style: ElevatedButton.styleFrom(
            backgroundColor: Config.primaryColor,
            disabledBackgroundColor: Config.primaryColor.withOpacity(0.12),
            foregroundColor: Colors.white,
            disabledForegroundColor: Config.primaryColor.withOpacity(0.38),
            shape: Config.elevenOutileButtonRadius,
          ),
          child: Text(
            title,
            style: const TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          )),
    );
  }
}

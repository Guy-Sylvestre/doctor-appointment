import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

class SocialButton extends StatelessWidget {
  const SocialButton({Key? key, required this.social}) : super(key: key);

  final String social;

  @override
  Widget build(BuildContext context) {
    Config().init(context);
    return OutlinedButton(
        onPressed: (){},
        style: OutlinedButton.styleFrom(
          padding: const EdgeInsets.symmetric(
            vertical: 15,
          ),
          shape: Config.elevenOutileButtonRadius,
          side: const BorderSide(width: 1, color: Colors.black)
        ),
        child: SizedBox(
          width: Config.widthSize! * 0.4,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(
                  "assets/socials/${social}.png",
                width: 40,
                height: 40,
              ),
              Text(
                social.toUpperCase(),
                style: const TextStyle(
                  color: Colors.black
                ),
              )
            ],
          ),
        ));
  }
}

//assets/socials/google.jpg
import 'package:doctor_apointment/presentation/auth/sign_in/sign_in_screen.dart';
import 'package:flutter/material.dart';

class Auth extends StatelessWidget {
  const Auth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SignInScreen();
  }
}

import 'package:doctor_apointment/presentation/widgets/button.dart';
import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

class SignInForm extends StatefulWidget {
  const SignInForm({Key? key}) : super(key: key);

  @override
  State<SignInForm> createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {

  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _passController = TextEditingController();
  bool obsecurePass = true;

  @override
  Widget build(BuildContext context) {
    return Form(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            TextFormField(
              controller: _emailController,
                keyboardType: TextInputType.emailAddress,
              cursorColor: Config.primaryColor,
              decoration: const InputDecoration(
                hintText: "Address Email",
                labelText: "Email",
                alignLabelWithHint: true,
                prefixIcon: Icon(Icons.email_outlined),
                prefixIconColor: Config.primaryColor,
              ),
            ),
            Config.spaceSmall,
            TextFormField(
              controller: _passController,
              keyboardType: TextInputType.visiblePassword,
              cursorColor: Config.primaryColor,
              obscureText: obsecurePass,
              decoration: InputDecoration(
                hintText: "Mot de passe",
                labelText: "Mot de passe",
                alignLabelWithHint: true,
                prefixIcon: const Icon(Icons.lock_clock_outlined),
                suffixIcon: IconButton(
                    onPressed: (){
                      setState(() {
                        obsecurePass = !obsecurePass;
                      });
                    },
                    icon: obsecurePass ?
                    const Icon(
                      Icons.visibility_off_outlined,
                      color: Colors.black38,
                    ): const Icon(
                      Icons.visibility_outlined,
                      color: Config.primaryColor,
                    )
                ),
                prefixIconColor: Config.primaryColor,
              ),
            ),
            Config.spaceSmall,
            Button(
                width: double.infinity,
                title: 'Connexion',
                // disable: true,
                press: (){
                  Navigator.pushNamed(context, "/main");
                }
            ),
          ],
        )
    );
  }
}

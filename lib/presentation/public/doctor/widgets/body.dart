import 'package:doctor_apointment/presentation/public/doctor/widgets/about.dart';
import 'package:doctor_apointment/presentation/public/doctor/widgets/signle.dart';
import 'package:doctor_apointment/presentation/widgets/button.dart';
import 'package:doctor_apointment/presentation/widgets/custom_appbar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../utils/constants/config.dart';

class Body extends StatefulWidget {
  Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool isFav = false;

  @override
  Widget build(BuildContext context) {
    Config().init(context);
    return Scaffold(
      appBar: CustomAppBar(
        appTitle: 'Doctor Details',
        icon: const FaIcon(Icons.arrow_back_ios),
        actions: [
          IconButton(
              onPressed: (){
                setState(() {
                  isFav = !isFav;
                });
              },
              icon: FaIcon(
                isFav ? Icons.favorite_rounded : Icons.favorite_outline,
                color: Colors.red,
              ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Column(
              children: [
                const AboutDoctor(),
                const DoctorSignle(),
                Padding(
                    padding: const EdgeInsets.all(20),
                  child: Button(
                      width: double.infinity,
                      title: "Prendre rendez-vous",
                      press: () => Navigator.pushNamed(context, "/booking"),
                  ),
                ),
                Config.spaceSmall,
              ],
            )
        ),
      ),
    );
  }
}

import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

import 'info.dart';

class DoctorSignle extends StatelessWidget {
  const DoctorSignle({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      margin: const EdgeInsets.only(bottom: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Config.spaceSmall,
          const DoctorInfo(),
          Config.spaceMedium,
          const Text(
            'A propos du médecin',
            style: TextStyle(
              fontWeight: FontWeight.w600,
            ),
          ),
          Config.spaceSmall,
          const Text(
              "Richard Tan est un dentiste expérimenté à Sarawak. Il est diplômé depuis 2008 et a terminé sa formation à l'hôpital général de Sungai Bulah.",
            style: TextStyle(
              fontWeight: FontWeight.w500,
              height: 1.5,
            ),
            softWrap: true,
            textAlign: TextAlign.justify,
          ),
        ],
      ),
    );
  }
}

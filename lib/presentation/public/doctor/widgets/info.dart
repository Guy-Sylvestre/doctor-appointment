import 'package:doctor_apointment/presentation/public/doctor/widgets/info_card.dart';
import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

class DoctorInfo extends StatelessWidget {
  const DoctorInfo({super.key});

  @override
  Widget build(BuildContext context) {
    Config().init(context);
    return const Row(
      children: [
        DoctorInfoCard(
            label: 'Patients',
            value: "106"
        ),
        SizedBox(width: 15,),
        DoctorInfoCard(
            label: 'Experiences',
            value: "10 years"
        ),
        SizedBox(width: 15,),
        DoctorInfoCard(
            label: 'Evaluation',
            value: "4.6"
        ),
      ],
    );
  }
}

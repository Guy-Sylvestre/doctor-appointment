import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../domaine/datas/home_categories.dart';
import '../../../../utils/constants/config.dart';

class Category extends StatelessWidget {
  const Category({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: Config.heightSize * 0.05,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: medCat.length,
          itemBuilder: (BuildContext context, int index){
            return Card(
              margin: const EdgeInsets.only(right: 15),
              color: Config.primaryColor,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 10,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    FaIcon(
                      medCat[index]['icon'],
                      color: Colors.white,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Text(
                        medCat[index]['category'],
                        style: const TextStyle(
                            fontSize: 16,
                            color: Colors.white
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
      ),
    );
  }
}

import 'package:doctor_apointment/presentation/public/appointment/widgets/card.dart';
import 'package:doctor_apointment/presentation/public/home/widgets/category.dart';
import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

import '../../../widgets/title_section.dart';
import '../../doctor/widgets/card.dart';
import 'header_title.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Config().init(context);
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 1,
        ),
        child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                const HeaderTile(),
                  Config.spaceMedium,
                  const TitleSection(title: "Catégorie"),
                  Config.spaceSmall,
                  const Category(),
                  Config.spaceSmall,
                  const TitleSection(title: "Rendez-vous aujourd'hui"),
                  Config.spaceSmall,
                  const CardAppoitment(),
                  Config.spaceSmall,
                  const TitleSection(title: "Les meilleurs médecins"),
                  Config.spaceSmall,
                  Column(
                    children: List.generate(10, (index) {
                      return const DoctorCard();
                    })
                  ),
                ],
              ),
            ),
        ),
      ),
    );
  }
}



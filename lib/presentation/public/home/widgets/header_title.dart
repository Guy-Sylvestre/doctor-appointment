import 'package:flutter/material.dart';

class HeaderTile extends StatelessWidget {
  const HeaderTile({super.key});

  @override
  Widget build(BuildContext context) {
    return const Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Sylvestre',
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(
          child: CircleAvatar(
            radius: 30,
            backgroundImage: AssetImage('assets/profile/guy_1.jpg'),
          ),
        ),
      ],
    );
  }
}

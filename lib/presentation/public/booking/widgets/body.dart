import 'package:doctor_apointment/presentation/widgets/button.dart';
import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';


class Body extends StatefulWidget {
  const Body({super.key});

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {

  CalendarFormat _format = CalendarFormat.month;
  DateTime _focusDay = DateTime.now();
  DateTime _currentDay = DateTime.now();
  int? _currentIndex;
  bool _isWeekend = false;
  bool _dateSelected = false;
  bool _timeSelected = false;

  @override
  Widget build(BuildContext context) {
    Config().init(context);
    return CustomScrollView(
      slivers: [
        SliverToBoxAdapter(
          child: Column(
            children: [
            //  display calendar
              _tableCalendar(),
              const Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 25,
                    horizontal: 10,
                  ),
                child: Text(
                  "Sélectionner l'heure de la consultation",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
        _isWeekend ? SliverToBoxAdapter(
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 30,
            ),
            alignment: Alignment.center,
            child: const Text(
              "Le week-end n'est pas disponible, veuillez choisir une autre date.",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                color: Colors.grey,
              ),
            ),
          ),
        ) : SliverGrid(
            delegate: SliverChildBuilderDelegate(
                    (context, index) {
                      return InkWell(
                        onTap: () {
                        //  Where select, update current index and set time selectd to true
                          setState(() {
                            _currentIndex = index;
                            _timeSelected = true;
                          });
                        },
                        child: Container(
                          margin: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: _currentIndex == index ? Colors.white : Colors.black,
                            ),
                            borderRadius: BorderRadius.circular(15),
                            color: _currentIndex == index ? Config.primaryColor : null,
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            "${index + 9}:00 ${index + 9 > 11 ? "PM" : "AM"}",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: _currentIndex == index ? Colors.white : null,
                            ),
                          ),
                        ),
                      );
                    },
              childCount: 8,
            ),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 4,
              childAspectRatio: 1.5
            ),
        ),
        SliverToBoxAdapter(
          child: Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 80,
            ),
            child: ElevatedButton(
                onPressed: _timeSelected && _dateSelected ? () => Navigator.pushNamed(context, "/success_appointement") : null,
                style: ElevatedButton.styleFrom(
                  backgroundColor: Config.primaryColor,
                  foregroundColor: Colors.white, disabledForegroundColor: Config.primaryColor.withOpacity(0.38), disabledBackgroundColor: Config.primaryColor.withOpacity(0.12),
                  shape: Config.elevenOutileButtonRadius,
                ),
                child: const Text(
                  "Prendre rendez-vous",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ))
          ),
        ),
      ],
    );
  }

//  Table calendar
Widget _tableCalendar() {
    return TableCalendar(
        focusedDay: _focusDay,
        firstDay: DateTime.now(),
        lastDay: DateTime(2023, 12, 31),
      calendarFormat: _format,
      currentDay: _currentDay,
      rowHeight: 48,
      calendarStyle: const CalendarStyle(
        todayDecoration: BoxDecoration(
          color: Config.primaryColor,
          shape: BoxShape.circle
        ),
      ),
      availableCalendarFormats: const {
          CalendarFormat.month: 'Month',
      },
      onFormatChanged: (format) {
          setState(() {
            _format = format;
          });
      },
      onDaySelected: ((selectedDay, focusDay) {
        setState(() {
          _currentDay = selectedDay;
          _focusDay = focusDay;
          _dateSelected = true;

          //Check if weekend is selected
          if (selectedDay.weekday == 6 || selectedDay.weekday == 7) {
            _isWeekend = true;
            _timeSelected = false;
            _currentIndex = null;
          }else{
            _isWeekend = false;
          }
        });
      }),
    );
}
}

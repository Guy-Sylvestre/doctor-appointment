import 'package:doctor_apointment/presentation/widgets/button.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 3,
                child: Image.asset("assets/success/successful-appointment.png"),
            ),
            Container(
              width: double.infinity,
              alignment: Alignment.center,
              child: Text(
                "Successfull",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const Spacer(),
            Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
              child: Button(
                  width: double.infinity,
                  title: "Retour a l'acceuil",
                  press: () => Navigator.pushNamed(context, "/main"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

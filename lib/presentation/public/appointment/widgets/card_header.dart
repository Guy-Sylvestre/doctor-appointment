import 'package:flutter/material.dart';

import '../../../../utils/constants/config.dart';

class CardHeaderShedule extends StatelessWidget {
  const CardHeaderShedule({super.key, required this.image, required this.category, required this.name, required this.colorName, required this.colorCategory});

  final String image;
  final String category;
  final String name;
  final Color colorName;
  final Color colorCategory;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        CircleAvatar(
          backgroundImage: AssetImage(image),
        ),
        const SizedBox(width: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              name,
              style: TextStyle(
                color: colorName,
                fontWeight: FontWeight.w700,
              ),
            ),
            const SizedBox(height: 5,),
            Text(
              category,
              style: TextStyle(
                color: colorCategory,
                fontWeight: FontWeight.w400,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

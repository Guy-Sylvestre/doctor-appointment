import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';


class SheduleCard extends StatelessWidget {
  const SheduleCard({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey.shade200,
        borderRadius: BorderRadius.circular(10),
      ),
      width: double.infinity,
      padding: EdgeInsets.all(20),
      child: const Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Icon(
            Icons.calendar_today,
            color: Config.primaryColor,
            size: 15,
          ),
          SizedBox(width: 5,),
          Text(
            'Monday, 11/28/2022',
            style: TextStyle(
                color: Config.primaryColor,
            ),
          ),
          SizedBox(width: 20,),
          Icon(
            Icons.access_alarm,
            color: Config.primaryColor,
            size: 17,
          ),
          SizedBox(width: 5,),
          Flexible(
              child: Text(
                '2:00 PM',
                style: TextStyle(
                    color: Config.primaryColor,
                ),
              )
          ),
        ],
      ),
    );
  }
}

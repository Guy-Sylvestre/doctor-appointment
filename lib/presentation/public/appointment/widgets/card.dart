import 'package:doctor_apointment/presentation/public/appointment/widgets/button_card.dart';
import 'package:doctor_apointment/presentation/public/appointment/widgets/card_header.dart';
import 'package:doctor_apointment/presentation/public/appointment/widgets/shedule_card.dart';
import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

class CardAppoitment extends StatelessWidget {
  const CardAppoitment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Config.primaryColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Material(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: [
              const CardHeaderShedule(
                  image: 'assets/doctor/doctor_1.png',
                  category: "Dental",
                  name: "Dr Richard Tan",
                  colorName: Colors.white,
                  colorCategory: Colors.black,
              ),
              Config.spaceSmall,
              const SheduleCard(),
              Config.spaceSmall,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ButtomCard(
                      press: (){},
                      bgColors: Colors.red,
                      textColors: Colors.white,
                      title: "Annuler"
                  ),
                  const SizedBox(width: 20,),
                  ButtomCard(
                      press: (){},
                      bgColors: Colors.blue,
                      textColors: Colors.white,
                      title: "Terminer"
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}


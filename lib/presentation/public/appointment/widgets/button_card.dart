import 'package:flutter/material.dart';

import '../../../../utils/constants/config.dart';

class ButtomCard extends StatelessWidget {
  const ButtomCard({super.key, required this.press, required this.bgColors, required this.textColors, required this.title});

  final Function press;
  final Color bgColors;
  final Color textColors;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: bgColors,
          side: const BorderSide(width: 1, color: Config.primaryColor),
          shape: Config.elevenOutileButtonRadius,
        ),
        onPressed: () => press(),
        child: Text(
          title,
          style: TextStyle(
              color: textColors,
          ),
        ),
      ),
    );
  }
}

import 'package:doctor_apointment/presentation/public/appointment/widgets/shedule_card.dart';
import 'package:doctor_apointment/utils/constants/config.dart';
import 'package:flutter/material.dart';

import 'button_card.dart';
import 'card_header.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

enum FilterStatus { upcoming, complete, cancel }

class _BodyState extends State<Body> {


  Alignment _alignment = Alignment.centerLeft;

  FilterStatus status = FilterStatus.upcoming;
  List schedules = [
    {
      "doctor_name": "Richard Tan",
      "doctor_profile": "assets/doctor/doctor_1.png",
      "category": "Soins dentaires",
      "status": FilterStatus.upcoming,
      // "status": "upcoming",
    },
    {
      "doctor_name": "Guy Sylvestre",
      "doctor_profile": "assets/doctor/doctor_2.png",
      "category": "Soins dentaires",
      "status": FilterStatus.cancel,
      // "status": "cancel",
    },
    {
      "doctor_name": "Medard Yves",
      "doctor_profile": "assets/doctor/doctor_3.png",
      "category": "Respiration",
      "status": FilterStatus.upcoming,
      // "status": "upcoming",
    },
    {
      "doctor_name": "Landry Kouadio",
      "doctor_profile": "assets/doctor/doctor_4.png",
      "category": "Général",
      "status": FilterStatus.complete,
      // "status": "complete",
    },
  ] ;

  @override
  Widget build(BuildContext context) {

    List filteredSchedules = schedules.where((shedule) {
      // switch (shedule['status']){
      //   case 'upcoming':
      //     shedule['status'] = FilterStatus.upcoming;
      //     break;
      //
      //   case 'complete':
      //     shedule['status'] = FilterStatus.complete;
      //     break;
      //
      //   case 'cancel':
      //     shedule['status'] = FilterStatus.cancel;
      //     break;
      // }
      return shedule['status'] == status;
    }).toList();

    return SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 20,
            top: 20,
            right: 20,
            bottom: 20
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Text(
                "Calendrier d'inscription",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold
                ),
              ),
              Config.spaceSmall,
              Stack(
                children: [
                  Container(
                    width: double.infinity,
                    height: 40,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (FilterStatus filterStatus in FilterStatus.values)
                          Expanded(
                              child: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    if (filterStatus == FilterStatus.upcoming){
                                      status = FilterStatus.upcoming;
                                      _alignment = Alignment.centerLeft;
                                    }else if (filterStatus == FilterStatus.complete){
                                      status = FilterStatus.complete;
                                      _alignment = Alignment.center;
                                    }else if (filterStatus == FilterStatus.cancel){
                                      status = FilterStatus.cancel;
                                      _alignment = Alignment.centerRight;
                                    }
                                  });
                                },
                                child: Center(
                                  child: Text(filterStatus.name),
                                ),
                              ),
                          ),
                      ],
                    ),
                  ),
                  AnimatedAlign(
                      alignment: _alignment,
                      duration: const Duration(microseconds: 200),
                    child: Container(
                      width: 100,
                      height: 40,
                      decoration: BoxDecoration(
                        color: Config.primaryColor,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Center(
                        child: Text(
                            status.name,
                          style: const TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Config.spaceSmall,
              Expanded(
                  child: ListView.builder(
                    itemCount: filteredSchedules.length,
                      itemBuilder: (BuildContext context, int index){
                        var _schedule = filteredSchedules[index];
                        bool isLastElement = filteredSchedules.length + 1 == index;
                        return Card(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            side: const BorderSide(
                              color: Colors.grey,
                            ),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          margin: !isLastElement ? const EdgeInsets.only(bottom: 20) : EdgeInsets.zero,
                          child: Padding(
                            padding: const EdgeInsets.all(15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                CardHeaderShedule(
                                  image: _schedule['doctor_profile'],
                                  category: "Dental",
                                  name: _schedule['doctor_name'],
                                  colorName: Colors.black,
                                  colorCategory: Colors.grey,
                                ),
                                const SizedBox(height: 15,),
                                const SheduleCard(),
                                const SizedBox(height: 15,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    ButtomCard(
                                        press: (){},
                                        bgColors: Colors.white,
                                        textColors: Config.primaryColor,
                                        title: "Annuler"
                                    ),
                                    const SizedBox(width: 20,),
                                    ButtomCard(
                                        press: (){},
                                        bgColors: Config.primaryColor,
                                        textColors: Colors.white,
                                        title: "Reporter"
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        );
                      }
                  ),
              ),
            ],
          ),
        )
    );
  }
}

// 3- 15min
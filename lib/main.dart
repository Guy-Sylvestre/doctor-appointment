import 'package:doctor_apointment/presentation/auth/auth_screen.dart';
import 'package:doctor_apointment/utils/theme/theme.dart';
import 'package:flutter/material.dart';

import 'config/router/router.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // this is for push navigator
  static final navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      title: 'Flutter Doctor App',
      debugShowCheckedModeBanner: false,
      theme: theme(),

      home: const Auth(),
      onGenerateRoute: onGenerateRoute,
    );
  }
}
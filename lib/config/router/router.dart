import 'package:doctor_apointment/presentation/layouts.dart';
import 'package:doctor_apointment/presentation/public/booking/booking_screen.dart';
import 'package:doctor_apointment/presentation/public/doctor/doctor_screen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../../presentation/public/home/home_screen.dart';
import '../../presentation/public/success_appointment/success_appointement_screen.dart';

Route<dynamic>? onGenerateRoute (settings){
  switch (settings.name) {
    case "/main":
      return PageTransition(
        child: const MainLayouts(),
        type: PageTransitionType.rightToLeft,
        settings: settings,
        duration: const Duration(milliseconds: 300),
        reverseDuration: const Duration(milliseconds: 300),
      );
      case "/home":
      return PageTransition(
        child: const HomeScreen(),
        type: PageTransitionType.rightToLeft,
        settings: settings,
        duration: const Duration(milliseconds: 300),
        reverseDuration: const Duration(milliseconds: 300),
      );
      case "/signle_doctor":
      return PageTransition(
        child: const DoctorScreen(),
        type: PageTransitionType.rightToLeft,
        settings: settings,
        duration: const Duration(milliseconds: 300),
        reverseDuration: const Duration(milliseconds: 300),
      );
      case "/booking":
      return PageTransition(
        child: const BookingScreen(),
        type: PageTransitionType.rightToLeft,
        settings: settings,
        duration: const Duration(milliseconds: 300),
        reverseDuration: const Duration(milliseconds: 300),
      );
      case "/success_appointement":
      return PageTransition(
        child: const SuccessAppointementSCreen(),
        type: PageTransitionType.rightToLeft,
        settings: settings,
        duration: const Duration(milliseconds: 300),
        reverseDuration: const Duration(milliseconds: 300),
      );
    default:
      return null;
  }
}
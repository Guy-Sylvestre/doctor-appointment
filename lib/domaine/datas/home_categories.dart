import 'package:font_awesome_flutter/font_awesome_flutter.dart';

List<Map<String, dynamic>> medCat = [
  {
    "icon": FontAwesomeIcons.userDoctor,
    "category": "General",
  },
  {
    "icon": FontAwesomeIcons.heartPulse,
    "category": "Cardiology",
  },
  {
    "icon": FontAwesomeIcons.lungs,
    "category": "Respirations",
  },
  {
    "icon": FontAwesomeIcons.hand,
    "category": "Dematology",
  },
  {
    "icon": FontAwesomeIcons.personPregnant,
    "category": "Gynecology",
  },
  {
    "icon": FontAwesomeIcons.teeth,
    "category": "Dental",
  },
];
class AppText {
  static final enText = {
    'welcome_text':  'Bienvenue',
    'signIn_text': 'Connectez-vous à votre compte',
    'registered_text': 'Vous avez déjà un compte?',
    'register_text': 'Vous pouvez facilement vous inscrire et vous connecter au médecin le plus proche de chez vous.',
    'signUp_text': "Vous n'avez pas de compte ?",
    "social_login": "Ou poursuivre avec le compte social",
    "forget_password": "Mot de passe Oublie ?",
    "sign_up": "S'inscrire",
  };
}
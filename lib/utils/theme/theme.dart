import 'package:flutter/material.dart';

import '../constants/config.dart';

ThemeData theme() {
  return ThemeData(
// pre-definie input decoration
    inputDecorationTheme: const InputDecorationTheme(
      focusColor: Config.primaryColor,
      border: Config.outilineBorder,
      focusedBorder: Config.focusBorder,
      errorBorder: Config.errorBorder,
      enabledBorder: Config.outilineBorder,
      floatingLabelStyle: TextStyle(color: Config.primaryColor),
      prefixIconColor: Colors.black38,
    ),
    scaffoldBackgroundColor: Colors.white,
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Config.primaryColor,
      selectedItemColor: Colors.white,
      showSelectedLabels: true,
      showUnselectedLabels: false,
      unselectedItemColor: Colors.grey.shade700,
      elevation: 10,
      type: BottomNavigationBarType.fixed,
    ),
    useMaterial3: true,
  );
}
